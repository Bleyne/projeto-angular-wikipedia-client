import {Component, Input, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WikipediaSearchService } from '../wikipedia-search.service';
import { Subject,  } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit {

  @Input() searchTerm: string;
  termoProcurado: string;
  assuntos: Array<string>;
  descricao: Array<string>;
  links: Array<string>;
  items: Array<string>;
  teste: Array<string>;
  term$ = new Subject<string>();
  constructor(private service: WikipediaSearchService) {
    this.term$
      .subscribe(term => this.search(term));
  }

  ngOnInit() {
  }

  search(term: string) {
    this.service.search(term)
      .subscribe(results => {
        this.termoProcurado = results[0],
        this.assuntos = results[1],
        this.descricao = results[2],
        this.links = results[3]
      });
  }


}
